function initFilter() {
  var $body = $('body');
  var applyButton = $('.js-filter-submit');

// Filter buttons

  $('.js-filter-button').click(function() {
    $body.removeClass('shows-all');
    $body.addClass('shows-filter');
    return false;
  })

  $('.js-all-button').click(function() {
    $body.removeClass('shows-filter');
    $body.addClass('shows-all');
    return false;
  })

// Checkboxes

$('.js-checkbox').click(function(event) {
  event.stopPropagation();
  window.event.cancelBubble = true
  applyButton.show();
})


}

initFilter()
