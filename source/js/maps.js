HTMLElement.prototype.hasClass = function(cls) {
    var i;
    var classes = this.className.split(" ");
    for(i = 0; i < classes.length; i++) {
        if(classes[i] == cls) {
            return true;
        }
    }
    return false;
};

var Balux = {
    initMap: function() {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(40.6700, -73.9400), // New York
            styles: this.mapStyles
        };

        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);

        Balux.initPins(map);
        Balux.initRoute(map, mapElement.hasClass('no-trail'));
    },

    initRoute: function(map, hideTrail) {
        var that = this;
        if (typeof route === 'undefined') return;
        var markers = [];
        var lat_lng = [];

        var marker = new google.maps.Marker({
            position: {
                lat: route.current_position.lat,
                lng: route.current_position.lng
            },
            map: map,
            icon: {
                anchor: new google.maps.Point(16, 16),
                url: Balux.personSVG(route.current_position.pin_color)
            }
        });

        route.locations.forEach(function(location) {
            if (!hideTrail) {
                lat_lng.push(
                    new google.maps.LatLng(location.position.lat, location.position.lng)
                )
            }

            var marker = new google.maps.Marker({
                position: location.position,
                map: map,
                icon: Balux.locationPin(location)
            });
            markers.push(marker);
            marker.addListener('click', function() {
                that.resetMarkersZIndex(markers);
                this.setZIndex(4);
                $('*[data-address-id="'+location.id+'"]').modal('toggle')
            });
        })

        if (route.history) {
            route.history.forEach(function(point) {
                console.log(point)
                var marker = new google.maps.Marker({
                    position: point.position,
                    map: map,
                    icon: Balux.historyPin()
                });
                markers.push(marker);
                marker.addListener('click', function() {
                    that.resetMarkersZIndex(markers);
                    this.setZIndex(2);
                    $('*[data-history-id="'+point.id+'"]').modal('toggle')
                });
            });
        }

        this.zoomToMarkers(map, markers);

        for (var t = 0; t < lat_lng.length; t++) {
            var service = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer();

            var bounds = new google.maps.LatLngBounds();
            var src = lat_lng[t];
            if ((t + 1) < lat_lng.length) {
                var des = lat_lng[t + 1];
                service.route({
                    origin: src,
                    destination: des,
                    travelMode: google.maps.DirectionsTravelMode.DRIVING
                }, function(result, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        var path = new google.maps.MVCArray();
                        var poly = new google.maps.Polyline({
                            map: map,
                            strokeColor: '#4986E7',
                            strokeWeight: 7
                        });
                        poly.setPath(path);
                        for (var k = 0, len = result.routes[0].overview_path.length; k < len; k++) {
                            path.push(result.routes[0].overview_path[k]);
                            bounds.extend(result.routes[0].overview_path[k]);
                            map.fitBounds(bounds);
                        }
                    } else console.log("Directions Service failed:" + status);
                });
            }
        }
    },

    historyPin: function() {
        var svg = 'data:image/svg+xml;utf-8,' + escape('<svg width="26" height="26" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg"><title>Oval</title><circle cx="1081" cy="411" r="11" transform="translate(-1068 -398)" stroke="#09B8FD" stroke-width="4" fill="#FCFCFC" fill-rule="evenodd"/></svg>');

        return {
            anchor: new google.maps.Point(13, 13),
            url: svg
        }
    },

    locationPin: function(location) {
        var loadingSvg = 'data:image/svg+xml;utf-8,' + escape('<svg width="40" height="40" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>waiting_loading</title><defs><circle id="a" cx="14" cy="14" r="12"/><mask id="b" x="-2" y="-2" width="28" height="28"><path fill="#fff" d="M0 0h28v28H0z"/><use xlink:href="#a"/></mask><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="e"><stop offset="100%"/><stop stop-color="#FFF" stop-opacity="0" offset="0%"/></linearGradient><circle id="d" cx="14" cy="14" r="14"/><filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="c"><feOffset dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"/><feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"/><feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0" in="shadowBlurOuter1"/></filter></defs><g transform="translate(4 2)" fill="none" fill-rule="evenodd"><use fill="#FFF" xlink:href="#a"/><use stroke="'+location.pin_color+'" mask="url(#b)" stroke-width="4" xlink:href="#a"/><circle fill="'+location.pin_color+'" cx="14" cy="14" r="9.75"/><g transform="matrix(1 0 0 -1 0 28)"><use fill="#000" filter="url(#c)" xlink:href="#d"/><use fill-opacity=".05" fill="url(#e)" xlink:href="#d"/></g><path d="M12.393 9.75h3.214v3.214h2.143L14 16.714l-3.75-3.75h2.143V9.75zm-2.143 8.036h7.5v1.07h-7.5v-1.07z" fill="#FFF"/></g></svg>');
        var unloadingSvg = 'data:image/svg+xml;utf-8,' + escape('<svg width="40" height="40" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>waiting_unloading</title><defs><circle id="a" cx="14" cy="14" r="12"/><mask id="b" x="-2" y="-2" width="28" height="28"><path fill="#fff" d="M0 0h28v28H0z"/><use xlink:href="#a"/></mask><linearGradient x1="50%" y1="100%" x2="50%" y2="0%" id="e"><stop stop-color="#FFF" stop-opacity="0" offset="0%"/><stop offset="100%"/></linearGradient><circle id="d" cx="14" cy="14" r="14"/><filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="c"><feOffset dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"/><feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"/><feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0" in="shadowBlurOuter1"/></filter></defs><g transform="translate(4 2)" fill="none" fill-rule="evenodd"><use fill="#FFF" xlink:href="#a"/><use stroke="'+location.pin_color+'" mask="url(#b)" stroke-width="4" xlink:href="#a"/><circle fill="'+location.pin_color+'" cx="14" cy="14" r="9.75"/><path d="M12.393 15.714h3.214V12.5h2.143L14 8.75l-3.75 3.75h2.143v3.214zm-2.143 1.072h7.5v1.07h-7.5v-1.07z" fill="#FFF"/><g transform="matrix(1 0 0 -1 0 28)"><use fill="#000" filter="url(#c)" xlink:href="#d"/><use fill-opacity=".05" fill="url(#e)" xlink:href="#d"/></g></g></svg>');
        var svg = location.is_loading ? loadingSvg : unloadingSvg;

        return {
            anchor: new google.maps.Point(14, 14),
            url: svg
        }
    },

    resetMarkersZIndex(markers) {
        markers.forEach(function(marker) {
            marker.setZIndex(1);
        });
    },

    initPins: function(map) {
        if (typeof pins === 'undefined') return;
        var markers = [], that = this;

        pins.forEach(function(pin) {
            var marker = new google.maps.Marker({
                position: pin.position,
                map: map,
                title: pin.driver_name,
                icon: Balux.pinSVG(pin, pin.pin_color)
            });
            markers.push(marker);

            marker.addListener('click', function() {
                that.resetMarkersZIndex(markers);
                this.setZIndex(2);
                that.showDriverPopup({
                    id: pin.id,
                    title: pin.driver_name,
                    temperature: pin.temperature,
                    kgs: pin.kgs,
                    pallets: pin.pallets,
                    boxes: pin.boxes,
                    car_number: pin.car_number,
                    pin_color: pin.pin_color,
                    url: pin.address_url
                });
            });
        });

        Balux.zoomToMarkers(map, markers);
    },

    showDriverPopup: function(popup) {
        console.log(popup);
        var $popup = $('.js-modal-popup');
        var $title = $('.js-modal-title');
        var $cancel = $('.js-modal-cancel');
        var $badge = $('.js-modal-badge');
        var $ok = $('.js-modal-ok');
        var $id = $('.js-modal-id');
        var $temperature = $('.js-modal-temperature');
        var $boxes = $('.js-modal-boxes');
        var $kgs = $('.js-modal-kgs');
        var $pallets = $('.js-modal-pallets');

        $temperature.text(popup.temperature);
        $kgs.text(popup.kgs);
        $boxes.text(popup.boxes);
        $pallets.text(popup.pallets);
        $id.text(popup.id);

        $title.text(popup.title);
        $badge.text(popup.car_number);
        $badge.css({'background-color': popup.pin_color});
        $ok.attr('href', popup.url);
        $popup.modal('toggle')
    },

    zoomToMarkers: function(map, markers) {
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }

        map.fitBounds(bounds);
    },

    personSVG: function(pinColor) {
        return 'data:image/svg+xml;utf-8,' + escape('<svg width="46" height="46" viewBox="0 0 46 46" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>Person Icon</title><defs><circle id="a" cx="19.002" cy="19.002" r="17.145"/><mask id="b" x="-2" y="-2" width="38.29" height="38.29"><path fill="#fff" d="M-.143-.143h38.29v38.29H-.143z"/><use xlink:href="#a"/></mask><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="e"><stop offset="100%"/><stop stop-color="#FFF" stop-opacity="0" offset="0%"/></linearGradient><path d="M19 38c10.493 0 19-8.507 19-19S29.493 0 19 0 0 8.507 0 19s8.507 19 19 19z" id="d"/><filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="c"><feOffset dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"/><feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"/><feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0" in="shadowBlurOuter1"/></filter></defs><g transform="translate(4 2)" fill="none" fill-rule="evenodd"><use fill="#FFF" xlink:href="#a"/><use stroke="'+pinColor+'" mask="url(#b)" stroke-width="4" xlink:href="#a"/><circle fill="'+pinColor+'" cx="19.001" cy="19.001" r="14.93"/><g transform="matrix(1 0 0 -1 0 38)"><use fill="#000" filter="url(#c)" xlink:href="#d"/><use fill-opacity=".05" fill="url(#e)" xlink:href="#d"/></g><path d="M19 10c1.987 0 3.6 1.613 3.6 3.6s-1.613 3.6-3.6 3.6-3.6-1.613-3.6-3.6S17.013 10 19 10zm0 16.933c-4 0-7.13-.89-8-2.666v-1.2c0-2.667 5.333-4.134 8-4.134s8 1.467 8 4.134v1.2c-.87 1.777-4 2.666-8 2.666z" fill="#FFF"/></g></svg>');
    },

    pinSVG: function(pin, color) {
        return {
            anchor: new google.maps.Point(35, 36),
            url: 'data:image/svg+xml;utf-8,' + escape('<svg width="70" height="74" viewBox="0 0 70 74" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>pin-car</title><defs><circle id="a" cx="34" cy="19" r="17.1"/><mask id="b" x="-2" y="-2" width="38.3" height="38.3"><path fill="#fff" d="M15 0H53V38H15z"/><use xlink:href="#a"/></mask><linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="e"><stop offset="100%"/><stop stop-color="#FFF" stop-opacity="0" offset="0%"/></linearGradient><circle id="d" cx="34" cy="19" r="19"/><filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="c"><feOffset dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"/><feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"/><feComposite in="shadowBlurOuter1" in2="SourceAlpha" operator="out" result="shadowBlurOuter1"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.5 0" in="shadowBlurOuter1"/></filter></defs><g transform="translate(0 2)" fill="none" fill-rule="evenodd"><use fill="#FFF" xlink:href="#a"/><use stroke="'+color+'" mask="url(#b)" stroke-width="4" xlink:href="#a"/><circle fill="'+color+'" cx="34" cy="19" r="14.9"/><path d="M40.2 13c-.2-.6-.7-1-1.3-1H29c-.5 0-1 .4-1.2 1L26 18v7c0 .6.4 1 1 1h.8c.5 0 1-.4 1-1v-.8h10.5v1c0 .4.4.8 1 .8h.8c.6 0 1-.4 1-1v-7l-1.8-5zm-11 8.6c-.8 0-1.4-.6-1.4-1.3 0-.7.6-1.3 1.3-1.3.8 0 1.4.6 1.4 1.3 0 .7-.6 1.3-1.3 1.3zm9.7 0c-.8 0-1.4-.6-1.4-1.3 0-.7.6-1.3 1.3-1.3.6 0 1.2.6 1.2 1.3 0 .7-.6 1.3-1.3 1.3zm-11.2-4.4l1.3-4h10l1.2 4H27.8z" fill="#FFF"/><g transform="matrix(1 0 0 -1 0 38)"><use fill="#000" filter="url(#c)" xlink:href="#d"/><use fill-opacity="0" fill="url(#e)" xlink:href="#d"/></g><path d="M0 54c0-2.2 1.8-4 4-4h62c2.2 0 4 1.8 4 4v14c0 2.2-1.8 4-4 4H4c-2.2 0-4-1.8-4-4V54z" fill="#141414" opacity=".6"/><text font-family="Roboto" font-size="11" font-weight="bold" fill="#FFF"><tspan x="13" y="65">'+pin.car_number+'</tspan></text></g></svg>')
        }
    },

    mapStyles: [
        {
            "featureType": "landscape",
            "stylers": [
                {
                    "hue": "#FFBB00"
                },
                {
                    "saturation": 43.4
                },
                {
                    "lightness": 37.6
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.highway",
            "stylers": [
                {
                    "hue": "#FFC200"
                },
                {
                    "saturation": -61.8
                },
                {
                    "lightness": 45.6
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "hue": "#FF0300"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 51.2
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "hue": "#FF0300"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 52
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {
                    "hue": "#0078FF"
                },
                {
                    "saturation": -13.2
                },
                {
                    "lightness": 2.4
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "hue": "#00FF6A"
                },
                {
                    "saturation": -1.0989010989011
                },
                {
                    "lightness": 11.2
                },
                {
                    "gamma": 1
                }
            ]
        }
    ]
}

google.maps.event.addDomListener(window, 'load', Balux.initMap);
